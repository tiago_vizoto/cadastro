class Pessoa:
    def __init__(self, nome, telefone, endereco):
        self.__nome = None
        self.__telefone = None
        self.__endereco = None

    def getNome(self):
        return self.__nome

    def getTelefone(self):
        return self.__telefone

    def getEndereco(self):
        return self.__endereco

    def setNome(self, nome):
        self.__nome = nome

    def setTelefone(self, telefone):
        self.__telefone = telefone

    def setEndereco(self, endereco):
        self.__endereco = endereco